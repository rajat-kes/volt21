package com.example.rajat.voit21.database;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;
import android.widget.EditText;

public class DatabaseHelper extends SQLiteOpenHelper {
    private static final String TAG="Volt21";

    private static String DATABASE_NAME ="user.db";

    //Table Name
    private static String TABLE_NAME="user";

    //Column Name
    private static String USER_ID="ID";
    private static String FIRST_NAME="FN";
    private static String SECOUND_NAME="SN";
    private static String GENDER="G";
    private static String EMAILID="EID";
    private static String PASSWORD="PASS";

    //Query for create Database Table
    private static String CREATE_TABLE="CREATE TABLE " + TABLE_NAME + "("
            + USER_ID + " INTEGER PRIMARY KEY AUTOINCREMENT," + FIRST_NAME + " TEXT,"
            + SECOUND_NAME + " TEXT," + GENDER + " TEXT," + EMAILID + " TEXT," + PASSWORD + " TEXT" + ")";

    //Query for Update database
    private static String DROP_TABLE="DROP TABLE IF EXISTS "+TABLE_NAME;

    public DatabaseHelper(Context context) {
        super(context, DATABASE_NAME, null, 1);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(CREATE_TABLE);

    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL(DROP_TABLE);
        onCreate(db);
    }

    //Database Creation is Completed


    //Store data in to database

    public void addData(EditText fn,EditText sn,EditText g,EditText eID,EditText pass){
        SQLiteDatabase db=this.getWritableDatabase();

        ContentValues cv=new ContentValues();
        cv.put(FIRST_NAME,fn.getText().toString().trim());
        cv.put(SECOUND_NAME,sn.getText().toString().trim());
        cv.put(GENDER,g.getText().toString().trim());
        cv.put(EMAILID,eID.getText().toString().trim());
        cv.put(PASSWORD,pass.getText().toString().trim());
        /*if((db.insert(TABLE_NAME,null,cv)>0)){
            db.close();
            return(true);
        }
        else{
            db.close();
            return(false);
        }*/
        db.insert(TABLE_NAME,null,cv);
                db.close();

    }

    public boolean checkUser(String email, String password) {
        Log.i(TAG, email+password);

        // array of columns to fetch
        String[] columns = {
                USER_ID
        };
        SQLiteDatabase db = this.getReadableDatabase();
        // selection criteria
        String selection = EMAILID + " = ?" + " AND " + PASSWORD + " = ?";

        // selection arguments
        String[] selectionArgs = {email, password};

        // query user table with conditions
        /**
         * Here query function is used to fetch records from user table this function works like we use sql query.
         * SQL query equivalent to this query function is
         * SELECT user_id FROM user WHERE user_email = 'jack@androidtutorialshub.com' AND user_password = 'qwerty';
         */
        Cursor cursor = db.query(TABLE_NAME, //Table to query
                columns,                    //columns to return
                selection,                  //columns for the WHERE clause
                selectionArgs,              //The values for the WHERE clause
                null,                       //group the rows
                null,                       //filter by row groups
                null);                      //The sort order

        int cursorCount = cursor.getCount();

        cursor.close();
        db.close();
        if (cursorCount > 0) {
            return true;
        }

        return false;
    }

}
