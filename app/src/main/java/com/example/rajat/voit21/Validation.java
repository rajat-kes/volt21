package com.example.rajat.voit21;

import android.annotation.TargetApi;
import android.content.Context;
import android.os.Build;
import android.support.annotation.RequiresApi;
import android.widget.EditText;
import android.widget.Toast;

public class Validation {

    private Context context;

    public Validation(Context context){
        this.context=context;
    }

    @TargetApi(Build.VERSION_CODES.LOLLIPOP)
    public boolean varifyLoginData(EditText email, EditText pass){
        String emailid=(String) email.getText().toString().trim();
        String password=(String) pass.getText().toString().trim();
        if(this.IsEmpty(email)){return false;}
        if(!this.ValidEmailId(email)){return  false;}
        if(this.IsEmpty(pass)){return false;}
        return true;
    }

    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    public boolean verifySignupdata(EditText fname, EditText lname, EditText gender, EditText email, EditText password, EditText cpassword){
        if(this.IsEmpty(fname)){return false;}
        if(this.IsEmpty(lname)){return false;}
        if(this.IsEmpty(gender)){return false;}
        if(this.IsEmpty(email)){return false;}
        if(this.IsEmpty(password)){return false;}
        if(this.IsEmpty(cpassword)){return false;}
        if(this.ValidEmailId(email)){return  false;}
        if(!this.MatchPassword(password,cpassword)){return false;}

        return true;
    }

    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    public boolean IsEmpty(EditText editText){
        String string=editText.getText().toString().trim();
        String name="Please Enter "+ editText.getTransitionName().trim();
        if(string.isEmpty()){
            Toast.makeText(context,name,Toast.LENGTH_LONG).show();
            return true;
        }
        return false;
    }

    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    public boolean ValidEmailId(EditText email){
        String string="Please Enter Correct "+ email.getTransitionName().trim();
        if(android.util.Patterns.EMAIL_ADDRESS.matcher(email.getText().toString().trim()).matches()){
            return true;
        }
        else{
            Toast.makeText(context,string,Toast.LENGTH_LONG).show();
            return false;
        }
    }
    public boolean MatchPassword(EditText epassword,EditText ecpassword){
        String password=epassword.getText().toString().trim();
        String cpassword=ecpassword.getText().toString().trim();
        String string="Password Not Match";
        if(password.equals(cpassword)){
            return true;
        }
        else{
            Toast.makeText(context,string,Toast.LENGTH_LONG).show();
            return false;
        }
    }
}
