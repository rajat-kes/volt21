package com.example.rajat.voit21;

import android.content.Context;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.example.rajat.voit21.database.DatabaseHelper;

public class Login extends AppCompatActivity {

    EditText emailid,password;
    Button login;
    TextView tvsignup;
    Validation validation;
    DatabaseHelper databaseHelper=new DatabaseHelper(this);

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        emailid=findViewById(R.id.etemail);
        password=findViewById(R.id.etpassword);
        login=findViewById(R.id.btlogin);
        tvsignup=findViewById(R.id.tv);
        validation=new Validation(this);
        login.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(validation.varifyLoginData(emailid,password)){
                    if(databaseHelper.checkUser(emailid.getText().toString().trim(),password.getText().toString().trim())){
                        Toast.makeText(Login.this,"Login successfully",Toast.LENGTH_LONG).show();
                        Intent intent=new Intent(Login.this,Home.class);
                        startActivity(intent);
                        finish();
                    }
                    else {
                        Toast.makeText(Login.this,"Login Unsuccessfully",Toast.LENGTH_LONG).show();
                    }
                }
            }
        });
        tvsignup.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent=new Intent(Login.this,Signup.class);
                startActivity(intent);
            }
        });
    }
}
