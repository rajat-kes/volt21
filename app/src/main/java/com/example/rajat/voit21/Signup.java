package com.example.rajat.voit21;

import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.support.annotation.RequiresApi;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.webkit.WebView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.example.rajat.voit21.database.DatabaseHelper;

public class Signup extends AppCompatActivity {
    TextView tvlogin;
    EditText fname,lname,gender,email,password,cpassword;
    Button signup;
    Validation validation=new Validation(this);
    DatabaseHelper databaseHelper=new DatabaseHelper(this);
    private  Context context;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_signup);
        tvlogin=findViewById(R.id.tvlogin);
        fname=findViewById(R.id.etfname);
        lname=findViewById(R.id.etlname);
        gender=findViewById(R.id.etgender);
        email=findViewById(R.id.etemail);
        password=findViewById(R.id.etpassword);
        cpassword=findViewById(R.id.etcpassword);
        signup=findViewById(R.id.btsingup);


        signup.setOnClickListener(new View.OnClickListener() {
            @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
            @Override
            public void onClick(View v) {
                validation.verifySignupdata(fname,lname,gender,email,password,cpassword);
                /*if(databaseHelper.addData(fname,lname,gender,email,password)){
                    Toast.makeText(context,"Data Store successfully",Toast.LENGTH_LONG).show();
                }
                else{
                    Toast.makeText(context,"Data Store Unsuccessfully",Toast.LENGTH_LONG).show();
                }*/
                databaseHelper.addData(fname,lname,gender,email,password);
                Intent intent=new Intent(Signup.this,Login.class);
                startActivity(intent);
                finish();

            }
        });
        tvlogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent=new Intent(Signup.this,Login.class);
                startActivity(intent);
                finish();
            }
        });

    }
}
